package TASK2;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Main tester = new Main();
        try {
            List<Student> readedStudends = tester.readJSON();
            highestGradeStudent(readedStudends);

        }
        catch (FileNotFoundException e){
            System.out.println("Error: "+e);
        }

    }

    private List<Student> readJSON() throws FileNotFoundException {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        Type listType = new TypeToken<ArrayList<Student>>(){}.getType();
        Gson gson = builder.create();
        BufferedReader bufferedReader = new BufferedReader(new FileReader("students.json"));
        List<Student> student = gson.fromJson(bufferedReader,listType);
        return student;
    }
    private static void highestGradeStudent(List<Student> studentsList){
        List<Double> averageGradeList = new ArrayList<>();
        for (int i = 0; i < studentsList.size(); i++) {
            List<Double> gradeList = new ArrayList<>();
            for (int j = 0; j < studentsList.get(i).getCourses().size(); j++) {
                double gradePart = studentsList.get(i).getCourses().get(j).getCourseGrade();
                gradeList.add(gradePart);
            }
            double averageGrade = calculateGrade(gradeList,gradeList.size());
            averageGradeList.add(averageGrade);
        }
        int highestIndex = highestValue(averageGradeList);
        System.out.println("Student with highest average grade is "+studentsList.get(highestIndex).getName()+". His average grade: "+averageGradeList.get(highestIndex));

    }
    private static double calculateGrade(List<Double> grades, int size){
        double sum = 0.0;
        for (int i = 0; i < grades.size(); i++) {
            double sum1 = grades.get(i);
            sum = sum1 + sum;
        }
        double average = (double) sum/size;
        return average;
    }
    private static int highestValue(List<Double> values){
        int index = 0;
        double highestVal = 0;
        for (int i = 0; i < values.size(); i++) {
            double comparedVal = values.get(i);
            if(comparedVal>highestVal){
                highestVal = comparedVal;
                index = i;
            }
        }
        return index;
    }
}
