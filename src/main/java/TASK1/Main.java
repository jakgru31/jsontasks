package TASK1;
import java.io.FileWriter;
import java.io.IOException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Movie> movies = new ArrayList<>();
        ArrayList<CastMember> cast1 = new ArrayList<>();
        cast1.add(new CastMember("Humphrey Bogart", "Rick Blaine"));
        cast1.add(new CastMember("Ingrid Bergman", "Ilsa Lund"));
        cast1.add(new CastMember("Paul Henreid", "Victor Laszlo"));
        cast1.add(new CastMember("Claude Rains", "Captain Louis Renault"));
        cast1.add(new CastMember("Conrad Veidt", "Major Heinrich Strasser"));
        movies.add(new Movie("Casablanca", "Michael Curtiz", 102, cast1));

        ArrayList<CastMember> cast2 = new ArrayList<>();
        cast2.add(new CastMember("Marlon Brando", "Don Vito Corleone"));
        cast2.add(new CastMember("Al Pacino", "Michael Corleone"));
        cast2.add(new CastMember("James Caan", "Sonny Corleone"));
        cast2.add(new CastMember("Robert Duvall", "Tom Hagen"));
        cast2.add(new CastMember("Diane Keaton", "Kay Adams"));
        movies.add(new Movie("The Godfather", "Francis Ford Coppola", 175, cast2));

        ArrayList<CastMember> cast3 = new ArrayList<>();
        cast3.add(new CastMember("Leonardo DiCaprio", "Cobb"));
        cast3.add(new CastMember("Joseph Gordon-Levitt", "Arthur"));
        cast3.add(new CastMember("Elliot Page", "Ariadne"));
        cast3.add(new CastMember("Tom Hardy", "Eames"));
        cast3.add(new CastMember("Ken Watanabe", "Saito"));
        movies.add(new Movie("Inception", "Christopher Nolan", 148, cast3));


        ArrayList<CastMember> cast4 = new ArrayList<>();
        cast4.add(new CastMember("Marilyn Monroe", "Sugar Kane Kowalczyk"));
        cast4.add(new CastMember("Tony Curtis", "Joe"));
        cast4.add(new CastMember("Jack Lemmon", "Jerry"));
        cast4.add(new CastMember("George Raft", "Spats Colombo"));
        cast4.add(new CastMember("Joe E. Brown", "Osgood Fielding III"));
        movies.add(new Movie("Some Like It Hot", "Billy Wilder", 121, cast4));


        Main tester = new Main();
        try {
            for (int i = 0; i < movies.size(); i++) {
                tester.writeJSON(movies);
            }
        }
        catch(IOException e) {
            System.out.println("Error: "+e);
        }

    }

    private void writeJSON(ArrayList<Movie> movies) throws IOException {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        Gson gson = builder.create();
        FileWriter writer = new FileWriter("movies.json");
        writer.write(gson.toJson(movies));
        writer.close();
    }
}