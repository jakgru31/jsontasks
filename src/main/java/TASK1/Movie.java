package TASK1;

import java.util.List;

public class Movie {
    private String title;
    private String directorName;
    private int duration;
    private List<CastMember> cast;

    public Movie(String title, String directorName, int duration, List<CastMember> cast) {
        this.title = title;
        this.directorName = directorName;
        this.duration = duration;
        try {
            if(cast.size() <2){
                throw new RuntimeException("Expected at least 2 cast members");
            }
            else {
                this.cast = cast;
            }
        }
        catch (Exception e){
            System.out.println("Error: "+e);
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDirectorName() {
        return directorName;
    }

    public void setDirectorName(String directorName) {
        this.directorName = directorName;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public List<CastMember> getCast() {
        return cast;
    }

    public void setCast(List<CastMember> cast) {
        this.cast = cast;
    }
}
