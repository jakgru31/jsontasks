package TASK3;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class JokeApiClient {
    public Joke jokeApiRequest(){
        Joke jokeObject = null;
        Gson gson = new Gson();
        try {
            URL apiURL = new URL("https://official-joke-api.appspot.com/random_joke");
            HttpURLConnection urlConnection = (HttpURLConnection) apiURL.openConnection();
            urlConnection.setRequestMethod("GET");
            int responseCode = urlConnection.getResponseCode();
            if(responseCode== HttpURLConnection.HTTP_OK){
                BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null){
                    stringBuilder.append(line);
                }
                reader.close();
                jokeObject = gson.fromJson(stringBuilder.toString(), Joke.class);
            }
            else {
                System.out.println("Response failed");
            }
        }
        catch (IOException e){
            System.out.println("Error: "+e);
        }
        if(jokeObject == null){
            throw new RuntimeException("Object is null");
        }
        return jokeObject;
    }
}
